package main

import (
	"context"
	_ "github.com/golang-migrate/migrate/v4/database/postgres"
	_ "github.com/golang-migrate/migrate/v4/source/github"
	"simpleApi/initiator"
)

// main is our api/application entrance point
func main() {
	initiator.Initiator(context.Background())
}
