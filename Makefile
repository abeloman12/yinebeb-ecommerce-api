startup:
	docker-compose up -d

migrateup:
	migrate -path internal/constants/query/schemas -database "cockroachdb://root@localhost:26257/simpleapi?sslmode=disable" -verbose up

migratedown:
	migrate -path internal/constants/query/schemas -database "cockroachdb://root@localhost:26257/simpleapi?sslmode=disable" -verbose down

migrateup_t:
	migrate -path internal/constants/query/schemas -database "cockroachdb://root@localhost:26257/simpleapi_test?sslmode=disable" -verbose up

migratedown_t:
	migrate -path internal/constants/query/schemas -database "cockroachdb://root@localhost:26257/simpleapi_test?sslmode=disable" -verbose down

sqlc:
	sqlc generate -f ./config/sqlc.yaml

swag:
	swag init -g initiator/initiator.go


test:
	go test -v -cover ./...

run:
	go run ./cmd/main.go

.PHONY: startup migrateup migratedown sqlc test run