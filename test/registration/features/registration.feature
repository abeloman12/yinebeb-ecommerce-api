Feature: registration
  As a new user
  I want to register by creating credential
  so that the system can remember me.

  Background:
    Given user on the homepage
    And user follows "registration"

  @first
  Scenario: successful new User creation
    When user enters credential details
      | first_name | last_name | email | password | role|
      | Yinebeb    | Tariku    | yin@gmail.com | test@123| admin|
    And user submit credential
    Then user will registered successfully

  @last
  Scenario Outline: unsuccessful new User creation
    When user enters credential details
      | first_name   | last_name   | email   | password   | role|
      | <first_name> | <last_name> | <email> | <password> | role|
    And user submit credential
    Then registration failed with field error message "<err_msg>"
    Examples:
      | first_name | last_name  |       email    | password  | role  |     err_msg         |
      |    Yinebeb |            | nuami@gmil.com | test@123  | user  | last name is required |
      |            |     Tariku | selam@gmil.com | test@123  | admin |  first name is required |
      |    Yinebeb |     Tariku |                | test@123  | user  |    email is required   |
      |    Yinebeb |       test | sooo@gmil.com  |           | user  |password is required    |
      |    Yinebeb |     Tariku | jems@gmil.com  |  testpass | admin | weak password      |