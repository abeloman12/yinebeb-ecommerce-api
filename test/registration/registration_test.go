package registration

import (
	"context"
	"encoding/json"
	"fmt"
	"github.com/cucumber/godog"
	"gitlab.com/2ftimeplc/2fbackend/bdd-testing-framework/src"
	"net/http"
	"simpleApi/internal/constants/model/dto"
	"simpleApi/test"
	"testing"
)

var url = "http://localhost:8083/v1/superAdmin/"

type createuserTest struct {
	test.TestInstance
	apiTest src.ApiTest
	user    struct {
		OK   bool     `json:"ok"`
		Data dto.User `json:"data"`
	}
}

func TestCreateuser(t *testing.T) {
	c := &createuserTest{}
	c.TestInstance = test.Initiate(context.Background(), "../../")
	c.apiTest.InitializeTest(t, "Create user test", "features/registration.feature", c.InitializeScenario)
}

func (c *createuserTest) userOnTheHomepage() error {
	return nil
}

func (c *createuserTest) userFollows(endpoint string) error {
	c.apiTest.URL = url + endpoint
	fmt.Println(c.apiTest.URL)
	return nil
}

func (c *createuserTest) userEntersCredentialDetails(table *godog.Table) (err error) {
	body, err := c.apiTest.ReadRow(table, nil, false)
	if err != nil {
		return err
	}
	c.apiTest.Body = body
	return nil
}

func (c *createuserTest) userSubmitCredential() error {
	c.apiTest.SendRequest()
	return nil
}

func (c *createuserTest) registrationFailedWithFieldErrorMessage(arg1 string) error {
	if err := c.apiTest.AssertStatusCode(http.StatusBadRequest); err != nil {
		return err
	}
	if err := c.apiTest.AssertStringValueOnPathInResponse("error.field_error.0.description", arg1); err != nil {
		return err
	}

	return nil
}

func (c *createuserTest) userWillRegisteredSuccessfully() error {
	if err := c.apiTest.AssertStatusCode(http.StatusOK); err != nil {
		return err
	}
	if err := json.Unmarshal(c.apiTest.ResponseBody, &c.user); err != nil {
		return err
	}

	if err := c.apiTest.AssertEqual(c.user.OK, true); err != nil {
		return err
	}

	if err := c.apiTest.AssertColumnExists("data.id"); err != nil {
		return err
	}
	if err := c.apiTest.AssertColumnExists("data.first_name"); err != nil {
		return err
	}
	if err := c.apiTest.AssertColumnExists("data.last_name"); err != nil {
		return err
	}
	if err := c.apiTest.AssertColumnExists("data.email"); err != nil {
		return err
	}
	return nil
}

func (c *createuserTest) InitializeScenario(ctx *godog.ScenarioContext) {
	ctx.Before(func(ctx context.Context, sc *godog.Scenario) (context.Context, error) {
		c.apiTest.Method = http.MethodPost
		c.apiTest.SetHeader("Content-Type", "application/json")
		c.apiTest.InitializeServer(c.Server) //does it call router.Run()
		return ctx, nil
	})

	ctx.Step(`^user enters credential details$`, c.userEntersCredentialDetails)
	ctx.Step(`^user follows "([^"]*)"$`, c.userFollows)
	ctx.Step(`^user on the homepage$`, c.userOnTheHomepage)
	ctx.Step(`^user submit credential$`, c.userSubmitCredential)
	ctx.Step(`^registration failed with field error message "([^"]*)"$`, c.registrationFailedWithFieldErrorMessage)
	ctx.Step(`^user will registered successfully$`, c.userWillRegisteredSuccessfully)
}
