package test

import (
	"context"
	"fmt"
	"os"
	"simpleApi/initiator"
	"simpleApi/internal/constants/dbinstance"
	"simpleApi/internal/handler/middleware"
	"simpleApi/platforms/logger"

	ginzap "github.com/gin-contrib/zap"
	"github.com/gin-gonic/gin"
	"github.com/spf13/viper"
)

type TestInstance struct {
	Server *gin.Engine
	DB     dbinstance.DBInstance
}

/*
for login and registration, this test initiator can work on a test databse-simpleapi_test
but for item, I tested by running from the simpelapi db; since add company feature file is not added.
*/
func Initiate(ctx context.Context, path string) TestInstance {
	log := logger.New(initiator.InitLogger())
	log.Info(context.Background(), "logger initialized")

	log.Info(context.Background(), "initializing config")
	configName := "config" // set this to test_config (optional);
	if name := os.Getenv("CONFIG_NAME"); name != "" {
		configName = name
		log.Info(context.Background(), fmt.Sprintf("config name is set to %s", configName))
	} else {
		log.Info(context.Background(), "using default config name 'config'")
	}
	initiator.InitConfig(configName, path+"config", log)
	log.Info(context.Background(), "config initialized")

	log.Info(context.Background(), "initializing database")
	Conn := initiator.InitDB(viper.GetString("database.url"), log)
	log.Info(context.Background(), "database initialized")

	enforcer := initiator.PermConfig(Conn, viper.GetString("config.modelt"))

	log.Info(context.Background(), "initializing persistence layer")
	dbConn := dbinstance.New(Conn)
	persistence := initiator.InitPersistence(dbConn, log)
	log.Info(context.Background(), "persistence layer initialized")

	log.Info(context.Background(), "initializing module")
	module := initiator.InitModule(persistence, log)
	log.Info(context.Background(), "module initialized")

	log.Info(context.Background(), "initializing handler")
	handler := initiator.InitHandler(module, log, viper.GetDuration("server.timeout"))
	log.Info(context.Background(), "handler initialized")

	log.Info(context.Background(), "initializing server")
	server := gin.New()
	server.Use(ginzap.RecoveryWithZap(log.GetZapLogger().Named("gin.recovery"), true))
	server.Use(middleware.ErrorHandler())
	log.Info(context.Background(), "server initialized")

	log.Info(context.Background(), "initializing router")
	v1 := server.Group("/v1")
	initiator.InitRouter(v1, handler, enforcer)
	log.Info(context.Background(), "router initialized")

	return TestInstance{
		Server: server,
		DB:     dbConn,
	}
}
