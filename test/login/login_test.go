package login

import (
	"context"
	"encoding/json"
	"github.com/cucumber/godog"
	"gitlab.com/2ftimeplc/2fbackend/bdd-testing-framework/src"
	"net/http"
	"simpleApi/internal/constants/model/dto"
	"simpleApi/test"
	"testing"
)

var url = "http://localhost:8083/v1/"
var err error

type loginUserTest struct {
	test.TestInstance
	apiTest src.ApiTest
	user    struct {
		OK   bool     `json:"ok"`
		Data dto.User `json:"data"`
	}
}

func TestLoginuser(t *testing.T) {
	c := &loginUserTest{}
	c.TestInstance = test.Initiate(context.Background(), "../../")
	c.apiTest.InitializeTest(t, "Login user test", "features/login.feature", c.InitializeScenario)
}

func (c *loginUserTest) userOnTheHomepage() error {
	return nil
}

func (c *loginUserTest) userFollows(endpoint string) error {
	c.apiTest.URL = url + endpoint
	return nil
}

func (c *loginUserTest) userEnterLoginCredential(table *godog.Table) error {
	body, err := c.apiTest.ReadRow(table, nil, false)
	if err != nil {
		return err
	}
	c.apiTest.Body = body
	return nil
}

func (c *loginUserTest) userSubmitCredential() error {
	c.apiTest.SendRequest()
	return nil
}

func (c *loginUserTest) userWillLoginSuccessfully() error {
	if err := c.apiTest.AssertStatusCode(http.StatusOK); err != nil {
		return err
	}
	if err := json.Unmarshal(c.apiTest.ResponseBody, &c.user); err != nil {
		return err
	}

	if err := c.apiTest.AssertEqual(c.user.OK, true); err != nil {
		return err
	}

	if err := c.apiTest.AssertColumnExists("name"); err != nil {
		return err
	}
	if err := c.apiTest.AssertColumnExists("token"); err != nil {
		return err
	}
	return nil
}

func (c *loginUserTest) userLoginWillFailWithError(arg1 string) error {
	if err := c.apiTest.AssertStatusCode(http.StatusBadRequest); err != nil {
		return err
	}
	if err := c.apiTest.AssertStringValueOnPathInResponse("error.field_error.0.description", arg1); err != nil {
		return err
	}
	return nil
}

func (c *loginUserTest) InitializeScenario(ctx *godog.ScenarioContext) {
	ctx.Before(func(ctx context.Context, sc *godog.Scenario) (context.Context, error) {
		c.apiTest.Method = http.MethodPost
		c.apiTest.SetHeader("Content-Type", "application/json")
		c.apiTest.InitializeServer(c.Server) //does it call router.Run()
		return ctx, nil
	})

	ctx.Step(`^user enter login credential$`, c.userEnterLoginCredential)
	ctx.Step(`^user follows "([^"]*)"$`, c.userFollows)
	ctx.Step(`^user login will fail with error "([^"]*)"$`, c.userLoginWillFailWithError)
	ctx.Step(`^user on the homepage$`, c.userOnTheHomepage)
	ctx.Step(`^user submit credential$`, c.userSubmitCredential)
	ctx.Step(`^user will login successfully$`, c.userWillLoginSuccessfully)
}
