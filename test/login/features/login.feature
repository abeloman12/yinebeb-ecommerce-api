Feature: login
  As a user
  I want to logged in
  So that I can access the system

  Background:
    Given user on the homepage
    And user follows "login"

  @first
  Scenario: Successful login
    When user enter login credential
      | email            | password |
      | aaa@gmail.com   | aaa@123 |
    And user submit credential
    Then user will login successfully

  @last
  Scenario Outline: unsuccessful login
    When user enter login credential
      | email   | password   |
      | <email> | <password> |
    And user submit credential
    Then user login will fail with error "<err_msg>"
    Examples:
    |           email |  password  |         err_msg |
    |  test@gmail.com |            | password is required |
    |                 |  test@123  |    email is required |
