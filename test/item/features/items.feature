Feature:items
  As an admin
  I want to add items
  So that I can keep the item log updated

  @first
  Scenario: adding item to item list
    Given I am logging with
      | email            | password |
      | bbb@gmail.com   | bbb@123 |
    When I add items to item table
     | item_name| price | made_in | image|
     | shoes    | 350   | Ethiopia| assets/image.jpeg|
    Then item list will upto date

  @last
  Scenario Outline: adding wrong item list
    Given I am logging with
      | email            | password |
      | bbb@gmail.com   | bbb@123 |
    When I add items to item table
      | item_name| price | made_in |image|
      | <item_name>  | <price> | <made_in> |<image>|
    Then item list will not updated due to field error "<err_msg>"
    Examples:
      | item_name | price | made_in | image            | err_msg               |
      |           | 350   |         | assets/test.jpeg | item name is required |
      | shoes     | 350   |         | assets/test.jpeg | made in is required   |

  @third
    Scenario: user add item
      Given I am logging with
        | email            | password |
        | aaa@gmail.com   | aaa@123 |
      When I add items to item table
        | item_name| price | made_in | image|
        | shoes    | 350   | Ethiopia|      assets/test.jpeg |
      Then add item will failed with error "You are not authorized"


