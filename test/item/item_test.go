package items

import (
	"context"
	"encoding/json"
	"fmt"
	"net/http"
	"simpleApi/internal/constants/model"
	"simpleApi/internal/constants/model/dto"
	"simpleApi/test"
	"testing"

	"github.com/cucumber/godog"
	"gitlab.com/2ftimeplc/2fbackend/bdd-testing-framework/src"
)

var url = "http://localhost:8082/v1/company/ec0fe6e7-c6dd-436d-85db-9f101ad381a6/addItem"
var err error
var Tokn string

type itemTest struct {
	test.TestInstance
	apiTest src.ApiTest
	user    struct {
		OK   bool     `json:"ok"`
		Data dto.User `json:"data"`
	}
}

func TestCreateitem(t *testing.T) {
	c := &itemTest{}
	c.TestInstance = test.Initiate(context.Background(), "../../")
	c.apiTest.InitializeTest(t, "Create user test", "features/items.feature", c.InitializeScenario)
}

// item feature, testInstance is done by considering the use-case person on the feature file are
// already registered with resp role.
func (c *itemTest) iAmLoggingWith(table *godog.Table) error {
	body, err := c.apiTest.ReadRow(table, nil, false)
	if err != nil {
		return err
	}
	c.apiTest.Body = body
	c.apiTest.URL = "http://localhost:8082/v1/company/ec0fe6e7-c6dd-436d-85db-9f101ad381a6/login"
	c.apiTest.SendRequest()
	ody := &model.Response{}

	data := c.apiTest.ResponseBody

	err = json.Unmarshal(data, &ody)
	Tokn = ""
	Tokn = Tokn + ody.Token
	fmt.Println(body)
	return err
}

func (c *itemTest) iAddItemsToItemTable(table *godog.Table) error {
	typee := []src.Type{
		{
			Column: "item_name",
			Kind:   0,
		},
		{
			Column: "price",
			Kind:   1,
		},
		{
			Column: "made_in",
			Kind:   0,
		},
		{
			Column: "image",
			Kind:   0,
		},
	}
	body, err := c.apiTest.ReadRow(table, typee, false)
	if err != nil {
		return err
	}
	c.apiTest.Body = body
	c.apiTest.URL = url
	var bearer = "Bearer "
	// Create a Bearer string by appending access token
	bearer = bearer + Tokn
	fmt.Println(bearer, typee)
	// add authorization Header to the req

	c.apiTest.SetHeader("Authorization", bearer)
	c.apiTest.URL = "http://localhost:8082/v1/company/ec0fe6e7-c6dd-436d-85db-9f101ad381a6/addItem"

	c.apiTest.SendRequest()
	fmt.Println("detect", body, c.apiTest.URL, string(c.apiTest.ResponseBody))
	return nil
}

func (c *itemTest) itemListWillUptoDate() error {
	if err := c.apiTest.AssertStatusCode(http.StatusOK); err != nil {
		return err
	}
	if err := json.Unmarshal(c.apiTest.ResponseBody, &c.user); err != nil {
		return err
	}

	if err := c.apiTest.AssertEqual(c.user.OK, true); err != nil {
		return err
	}

	if err := c.apiTest.AssertColumnExists("data.item_name"); err != nil {
		return err
	}
	if err := c.apiTest.AssertColumnExists("data.price"); err != nil {
		return err
	}
	if err := c.apiTest.AssertColumnExists("data.made_in"); err != nil {
		return err
	}
	return nil
}

func (c *itemTest) addItemWillFailedWithError(arg1 string) error {
	if err := c.apiTest.AssertStatusCode(http.StatusBadRequest); err != nil {
		return err
	}
	if err := c.apiTest.AssertStringValueOnPathInResponse("error.field_error.0.description", arg1); err != nil {
		return err
	}
	return nil
}

func (c *itemTest) itemListWillNotUpdatedDueToFieldError(arg1 string) error {
	if err := c.apiTest.AssertStatusCode(http.StatusBadRequest); err != nil {
		return err
	}
	if err := c.apiTest.AssertStringValueOnPathInResponse("error.field_error.0.description", arg1); err != nil {
		return err
	}
	return nil
}

func (c *itemTest) InitializeScenario(ctx *godog.ScenarioContext) {
	ctx.Before(func(ctx context.Context, sc *godog.Scenario) (context.Context, error) {
		c.apiTest.Method = http.MethodPost
		c.apiTest.URL = "http://localhost:8082/v1/company/4d1a57e6-6442-401a-8cef-8210cf8024e4/addItem"
		c.apiTest.SetHeader("Content-Type", "application/json")
		c.apiTest.InitializeServer(c.Server) //does it call router.Run()
		return ctx, nil
	})

	ctx.Step(`^add item will failed with error "([^"]*)"$`, c.addItemWillFailedWithError)
	ctx.Step(`^I add items to item table$`, c.iAddItemsToItemTable)
	ctx.Step(`^I am logging with$`, c.iAmLoggingWith)
	ctx.Step(`^item list will not updated due to field error "([^"]*)"$`, c.itemListWillNotUpdatedDueToFieldError)
	ctx.Step(`^item list will upto date$`, c.itemListWillUptoDate)
}
