# simpleRestApi

## Description
Simple restful api project which demonstrate 
  - **gin** web framework
  - working with database(**cockroachDB**) CRUD operations
  - **RBAC** with domain exemplified
  - Support **multi-tenant(role)** company structures
  - JWT Beare-token based authentication
  - **Hexa-DDD/clean** architecture applied
  - well commented code 
  - **Swagger** docs added for api handlers
  - following **BDD** along with **agile** methodology
  - unit testing
  - ...

## Installation
  **To run and check it on**
  
  **CockroachDB**
   - You can follow the make file, simply run them sequentially.
   - Ensure you install all required packages and tools like golang-migrate

   - To install migrate for cockroachdb, run the command with the appropriate tag 
     ```bash
     go install -tags 'cockroachdb' github.com/golang-migrate/migrate/v4/cmd/migrate@latest
     ```

  ### Docker-compose
  - Copy belows line of code onto a file named, _docker-compose.yml_
```bash
version: '3.5'

services:
crdb:
image: cockroachdb/cockroach:v22.1.8
ports:
- "26257:26257"
- "8080:8080"
command: start-single-node --insecure
volumes:
- "${PWD}/cockroach-data/crdb:/cockroach/cockroach-data"
restart: always
```
  - from the dir where the .yml file exist, run   
     ```bash
    docker-compose up -d
    ```
  - Connect to a database viewer such as tablePlus to have a nice view onto database. 

## Documentation
You can get **Swagger** based docs [here](http://localhost:8082/swagger/index.html)                                                                                                                                

**_NB:_** Ensure you run the server before looking for docs.

## Author
  - [Yinebeb T.](https://gitlab.com/Yinebeb-01)