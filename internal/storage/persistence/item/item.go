package item

import (
	"context"
	"github.com/google/uuid"
	"go.uber.org/zap"
	"simpleApi/internal/constants/dbinstance"
	"simpleApi/internal/constants/errors"
	"simpleApi/internal/constants/model/db"
	"simpleApi/internal/constants/model/dto"
	"simpleApi/internal/storage"
	"simpleApi/platforms/logger"
	"time"
)

type item struct {
	db  dbinstance.DBInstance
	log logger.Logger
}

func Init(db dbinstance.DBInstance, log logger.Logger) storage.Item {
	return &item{
		db:  db,
		log: log,
	}
}
func (i *item) Create(ctx context.Context, param dto.AddItem) (*dto.Item, error) {
	item, err := i.db.AddItem(ctx, db.AddItemParams{
		CompanyID: param.CompanyID,
		Price:     param.Price,
		ItemName:  param.ItemName,
		MadeIn:    param.MadeIn,
		Image:     param.Image,
	})
	if err != nil {
		err = errors.ErrWriteError.Wrap(err, "could not create Item")
		i.log.Error(ctx, "unable to create item", zap.Error(err), zap.Any("item", param))
		return nil, err
	}
	return &dto.Item{
		ID:        item.ID,
		ItemName:  item.ItemName,
		Price:     item.Price,
		MadeIn:    item.MadeIn,
		Image:     item.Image,
		CreatedAt: item.CreatedAt,
	}, nil
}

func (i *item) Get(ctx context.Context, id uuid.UUID) (*dto.Item, error) {
	item, err := i.db.GetItem(ctx, id)
	if err != nil {
		err = errors.ErrWriteError.Wrap(err, "could not read item")
		i.log.Error(ctx, "unable to get item", zap.Error(err))
		return nil, err
	}
	return &dto.Item{
		ID:        item.ID,
		ItemName:  item.ItemName,
		Price:     item.Price,
		MadeIn:    item.MadeIn,
		Image:     item.Image,
		CreatedAt: item.CreatedAt,
		UpdatedAt: item.UpdatedAt,
	}, nil
}

func (it *item) GetAll(ctx context.Context, companyId uuid.UUID) ([]dto.Item, error) {
	items, err := it.db.ListItem(ctx, companyId)
	if err != nil {
		err = errors.ErrWriteError.Wrap(err, "could not read item")
		it.log.Error(ctx, "unable to get item", zap.Error(err))
		return nil, err
	}
	var dtoItems []dto.Item
	for _, item := range items {
		var i dto.Item
		i.ID = item.ID
		i.CompanyID = item.CompanyID
		i.Price = item.Price
		i.MadeIn = item.MadeIn
		i.CreatedAt = item.CreatedAt
		i.UpdatedAt = item.UpdatedAt
		dtoItems = append(dtoItems, i)
	}
	return dtoItems, nil
}

func (i *item) Update(ctx context.Context, param dto.UpdateItem) (*dto.Item, error) {
	item, err := i.db.UpdateItem(ctx, db.UpdateItemParams{
		ID:        param.ID,
		Price:     param.Price,
		UpdatedAt: time.Now(),
	})
	if err != nil {
		err = errors.ErrWriteError.Wrap(err, "could not update item")
		i.log.Error(ctx, "unable to update item", zap.Error(err), zap.Any("user", param))
		return nil, err
	}
	return &dto.Item{
		ID:        item.ID,
		Price:     item.Price,
		ItemName:  item.ItemName,
		MadeIn:    item.MadeIn,
		Image:     item.Image,
		CreatedAt: item.CreatedAt,
	}, nil
}
