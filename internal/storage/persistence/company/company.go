package company

import (
	"context"
	"github.com/google/uuid"
	"go.uber.org/zap"
	"simpleApi/internal/constants/dbinstance"
	"simpleApi/internal/constants/errors"
	"simpleApi/internal/constants/model/db"
	"simpleApi/internal/constants/model/dto"
	"simpleApi/internal/storage"
	"simpleApi/platforms/logger"
)

type company struct {
	db  dbinstance.DBInstance
	log logger.Logger
}

func Init(db dbinstance.DBInstance, log logger.Logger) storage.Company {
	return &company{
		db:  db,
		log: log,
	}
}
func (c *company) Create(ctx context.Context, param dto.AddCompany) (*dto.Company, error) {
	company, err := c.db.AddCompany(ctx, db.AddCompanyParams{
		Name:    param.Name,
		Address: param.Address,
	})
	if err != nil {
		err = errors.ErrWriteError.Wrap(err, "could not create company")
		c.log.Error(ctx, "unable to create company", zap.Error(err), zap.Any("company", param))
		return nil, err
	}
	return &dto.Company{
		ID:      company.ID,
		Name:    company.Name,
		Address: company.Address,
	}, nil
}

func (c *company) Get(ctx context.Context, id uuid.UUID) (*dto.Company, error) {
	company, err := c.db.GetCompany(ctx, id)
	if err != nil {
		err = errors.ErrWriteError.Wrap(err, "could not read company")
		c.log.Error(ctx, "unable to get company", zap.Error(err))
		return nil, err
	}
	return &dto.Company{
		ID:      company.ID,
		Name:    company.Name,
		Address: company.Address,
	}, nil
}

func (c *company) GetAll(ctx context.Context) ([]dto.Company, error) {
	companies, err := c.db.ListCompany(ctx)
	if err != nil {
		err = errors.ErrWriteError.Wrap(err, "could not read companies")
		c.log.Error(ctx, "unable to get companies", zap.Error(err))
		return nil, err
	}
	var dtoCOmpanies []dto.Company
	for _, company := range companies {
		var i dto.Company
		i.ID = company.ID
		i.Name = company.Name
		i.Address = company.Address
		dtoCOmpanies = append(dtoCOmpanies, i)
	}
	return dtoCOmpanies, nil
}
