package user

import (
	"context"
	"go.uber.org/zap"
	"simpleApi/internal/constants/dbinstance"
	"simpleApi/internal/constants/errors"
	"simpleApi/internal/constants/model/db"
	"simpleApi/internal/constants/model/dto"
	"simpleApi/internal/storage"
	"simpleApi/platforms/logger"
)

type user struct {
	db  dbinstance.DBInstance
	log logger.Logger
}

func Init(db dbinstance.DBInstance, log logger.Logger) storage.User {
	return &user{
		db:  db,
		log: log,
	}
}
func (u *user) Create(ctx context.Context, param dto.CreateUser) (*dto.User, error) {
	userr, err := u.db.CreateUser(ctx, db.CreateUserParams{
		Email:          param.Email,
		FirstName:      param.FirstName,
		LastName:       param.LastName,
		HashedPassword: param.Password,
	})
	if err != nil {
		err = errors.ErrWriteError.Wrap(err, "could not create user")
		u.log.Error(ctx, "unable to create user", zap.Error(err), zap.Any("user", param))
		return nil, err
	}
	return &dto.User{
		ID:        userr.ID,
		Email:     userr.Email,
		FirstName: userr.FirstName,
		LastName:  userr.LastName,
		Password:  userr.HashedPassword,
		CreatedAt: userr.CreatedAt,
	}, nil
}

func (u *user) Get(ctx context.Context, email string) (*dto.User, error) {
	user, err := u.db.GetUser(ctx, email)
	if err != nil {
		err = errors.ErrWriteError.Wrap(err, "could not read user")
		u.log.Error(ctx, "unable to get user", zap.Error(err))
		return nil, err
	}
	return &dto.User{
		ID:        user.ID,
		Email:     user.Email,
		FirstName: user.FirstName,
		LastName:  user.LastName,
		CreatedAt: user.CreatedAt,
		Password:  user.HashedPassword,
		UpdatedAt: user.UpdatedAt,
	}, nil
}

func (u *user) UpdateRole(ctx context.Context, param dto.UpdateRole) (*dto.User, error) {
	user, err := u.db.UpdateRole(ctx, db.UpdateRoleParams{
		ID:   param.ID,
		Role: param.Role,
	})
	if err != nil {
		err = errors.ErrWriteError.Wrap(err, "could not update role")
		u.log.Error(ctx, "unable to update role", zap.Error(err), zap.Any("user-role", param))
		return nil, err
	}
	return &dto.User{
		ID:        user.ID,
		Email:     user.Email,
		FirstName: user.FirstName,
		LastName:  user.LastName,
		Password:  user.HashedPassword,
		CreatedAt: user.CreatedAt,
	}, nil
}
