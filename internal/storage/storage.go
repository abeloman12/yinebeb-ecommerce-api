package storage

import (
	"context"
	"github.com/google/uuid"
	"simpleApi/internal/constants/model/dto"
)

type User interface {
	Create(ctx context.Context, param dto.CreateUser) (*dto.User, error)
	Get(ctx context.Context, email string) (*dto.User, error)
	UpdateRole(ctx context.Context, param dto.UpdateRole) (*dto.User, error)
}

type Item interface {
	Create(ctx context.Context, param dto.AddItem) (*dto.Item, error)
	GetAll(ctx context.Context, companyID uuid.UUID) ([]dto.Item, error)
	Get(ctx context.Context, id uuid.UUID) (*dto.Item, error)
	Update(ctx context.Context, param dto.UpdateItem) (*dto.Item, error)
}

type Company interface {
	Create(ctx context.Context, param dto.AddCompany) (*dto.Company, error)
	GetAll(ctx context.Context) ([]dto.Company, error)
	Get(ctx context.Context, id uuid.UUID) (*dto.Company, error)
}
