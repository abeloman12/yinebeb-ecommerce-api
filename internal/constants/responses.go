package constants

import (
	"simpleApi/internal/constants/model"
	"simpleApi/internal/constants/model/dto"

	"github.com/gin-gonic/gin"
)

func SuccessResponse(ctx *gin.Context, statusCode int, data interface{}) {
	ctx.JSON(
		statusCode,
		model.Response{
			OK:   true,
			Data: data,
		},
	)
}

func LoginSuccessResponse(ctx *gin.Context, statusCode int, logResp dto.LogInResponse) {
	ctx.JSON(
		statusCode,
		model.Response{
			OK:    true,
			Name:  logResp.Name,
			Token: logResp.Token,
		},
	)
}

func ErrorResponse(ctx *gin.Context, err *model.ErrorResponse) {
	ctx.AbortWithStatusJSON(err.Code, model.Response{
		OK:    false,
		Error: err,
	})
}
