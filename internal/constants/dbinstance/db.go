package dbinstance

import (
	"github.com/jackc/pgx/v4/pgxpool"
	"simpleApi/internal/constants/model/db"
)

type DBInstance struct {
	*db.Queries
	Pool *pgxpool.Pool
}

// NewStore returns new Store which extend Queries.
func New(pool *pgxpool.Pool) DBInstance {
	return DBInstance{
		Pool:    pool,
		Queries: db.New(pool),
	}
}
