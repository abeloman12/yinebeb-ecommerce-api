-- name: AddItem :one
INSERT INTO items (
    company_id,
    item_name,
    price,
    made_in,
    image
)
VALUES (
           $1, $2, $3,$4,$5
       ) RETURNING *;

-- name: GetItem :one
SELECT * FROM items
WHERE id = $1 LIMIT 1;

-- name: ListItem :many
SELECT * FROM items
WHERE company_id = $1
ORDER BY id;

-- name: UpdateItem :one
UPDATE items
SET price = $2, updated_at = $3
WHERE id = $1
    RETURNING *;
