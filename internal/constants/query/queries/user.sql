-- name: CreateUser :one
INSERT INTO users (
                   first_name,
                   last_name,
                   email,
                   role,
                   hashed_password
                   )
VALUES (
        $1, $2, $3, $4,$5
        ) RETURNING *;

-- name: GetUser :one
SELECT * FROM users
WHERE email = $1 LIMIT 1;

-- name: UpdateRole :one
UPDATE users
SET role = $2
WHERE id = $1
    RETURNING *;