-- name: AddCompany :one
INSERT INTO company (
    name,
    address
)
VALUES (
           $1, $2
       ) RETURNING *;

-- name: GetCompany :one
SELECT * FROM company
WHERE id = $1 LIMIT 1;

-- name: ListCompany :many
SELECT * FROM company
ORDER BY id;