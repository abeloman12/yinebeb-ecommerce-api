-- CREATE SEQUENCE u_seq START 1 INCREMENT 1;
CREATE TABLE "users" (
                         "id" UUID PRIMARY KEY DEFAULT gen_random_uuid() ,
                         "first_name" varchar NOT NULL,
                         "last_name" varchar NOT NULL,
                         "email" varchar UNIQUE NOT NULL,
                         "role" varchar NOT NULL,
                         "hashed_password" varchar NOT NULL,
                         "created_at" timestamptz NOT NULL DEFAULT ((now())),
                         "updated_at"  timestamptz NOT NULL DEFAULT ((now()))
);

CREATE INDEX ON "users" ("email");

-- CREATE SEQUENCE i_seq START 1 INCREMENT 1;
CREATE TABLE "items" (
                         "id" UUID PRIMARY KEY DEFAULT gen_random_uuid() ,
                         "company_id" UUID NOT NULL,
                         "item_name" varchar NOT NULL,
                         "price" DECIMAL NULL,
                         "made_in" varchar NOT NULL,
                         "image" varchar not null,
                         "created_at" timestamptz NOT NULL DEFAULT ((now())),
                         "updated_at"  timestamptz NOT NULL DEFAULT ((now()))
);

CREATE INDEX ON "items" ("item_name");

CREATE TABLE "company" (
                         "id" UUID PRIMARY KEY DEFAULT gen_random_uuid() ,
                         "name" varchar NOT NULL,
                         "address" varchar NOT NULL,
                         "created_at" timestamptz NOT NULL DEFAULT ((now())),
                         "updated_at"  timestamptz NOT NULL DEFAULT ((now()))
);

CREATE INDEX ON "company" ("name");

ALTER TABLE "items" ADD FOREIGN KEY ("company_id") REFERENCES "company" ("id") ON DELETE CASCADE;
