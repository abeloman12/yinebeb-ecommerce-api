package dto

import (
	"github.com/google/uuid"
	"time"
)

type User struct {
	// ID is the unique identifier of the user.
	ID uuid.UUID `json:"id"`
	// FirstName is the first name of the user.
	FirstName string `json:"first_name,omitempty"`
	LastName  string `json:"last_name,omitempty"`
	// Email is the email of the user.
	Email    string `json:"email,omitempty"`
	Password string `json:"password,omitempty"`
	// CreatedAt is the time when the user is created.
	// It is automatically set when the user is created.
	CreatedAt time.Time `json:"created_at,omitempty"`
	// DeletedAt is the time the user was deleted.
	UpdatedAt time.Time `json:"updated_at,omitempty"`
}

type CreateUser struct {
	FirstName string `json:"first_name,omitempty"`
	LastName  string `json:"last_name,omitempty"`
	Email     string `json:"email,omitempty"`
	Password  string `json:"password,omitempty"`
	Role      string `json:"role"`
}

type UserResponse struct {
	ID        string    `json:"id"`
	FirstName string    `json:"first_name"`
	LastName  string    `json:"last_name"`
	Email     string    `json:"email"`
	CreatedAt time.Time `json:"created_at"`
}

type LoginUserRequest struct {
	Email    string `json:"email,omitempty"`
	Password string `json:"password,omitempty"`
}

type LogInResponse struct {
	Name  string `json:"name"`
	Token string `json:"token"`
}

type UpdateUserRoleRequest struct {
	ID   string `json:"id,omitempty"`
	Role string `json:"role,omitempty"`
}

type UpdateRole struct {
	ID   uuid.UUID `json:"id"`
	Role string    `json:"role"`
}
