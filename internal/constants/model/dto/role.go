package dto

type AddGroupPolicy struct {
	ID     string `json:"id" binding:"required"`
	Role   string `json:"role" binding:"required"`
	Domain string `json:"domain" binding:"required"`
}

type AddPolicyRequest struct {
	Role   string `json:"role" binding:"required"`
	Domain string `json:"domain" binding:"required"`
	Object string `json:"object" binding:"required"`
	Action string `json:"action" binding:"required"`
}
