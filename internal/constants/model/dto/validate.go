package dto

import (
	"simpleApi/utils"

	validation "github.com/go-ozzo/ozzo-validation/v4"
	"github.com/go-ozzo/ozzo-validation/v4/is"
)

// user struct validation
func (u CreateUser) Validate() error {
	return validation.ValidateStruct(&u,
		validation.Field(&u.FirstName, validation.Required.Error("first name is required")),
		validation.Field(&u.LastName, validation.Required.Error("last name is required")),
		validation.Field(&u.Email, validation.Required.Error("email is required"), is.EmailFormat.Error("email is not valid")),
		validation.Field(&u.Password, validation.Required.Error("password is required"), PassWord.Error("weak password")),
	)
}

var ErrPass = validation.NewError("validation_is_password", "must be strong password")
var PassWord = validation.NewStringRuleWithError(utils.IsValid, ErrPass)

func (u LoginUserRequest) Validate() error {
	return validation.ValidateStruct(&u,
		validation.Field(&u.Email, validation.Required.Error("email is required")),
		validation.Field(&u.Password, validation.Required.Error("password is required")),
	)
}

func (u UpdateRole) Validate() error {
	return validation.ValidateStruct(&u,
		validation.Field(&u.ID, validation.Required.Error("id is required")),
		validation.Field(&u.Role, validation.Required.Error("role is required")),
	)
}

// Validate item structs
func (u AddItemRequest) Validate() error {
	return validation.ValidateStruct(&u,
		validation.Field(&u.ItemName, validation.Required.Error("item name is required")),
		//validation.Field(&u.Price, validation.Required.Error("price is required")),
		validation.Field(&u.MadeIn, validation.Required.Error("made in is required")),
		validation.Field(&u.Image, validation.Required.Error("image is required")),
	)
}
func (u UpdateItemRequest) Validate() error {
	return validation.ValidateStruct(&u,
		validation.Field(&u.ID, validation.Required.Error("iD is required")),
		validation.Field(&u.Price, validation.Required.Error("price is required")),
	)
}

// validate company struct
func (u AddCompany) Validate() error {
	return validation.ValidateStruct(&u,
		validation.Field(&u.Name, validation.Required.Error("name is required")),
		validation.Field(&u.Address, validation.Required.Error("Address is required")),
	)
}
