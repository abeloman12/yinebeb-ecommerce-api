package dto

import (
	"github.com/google/uuid"
)

type Company struct {
	ID      uuid.UUID `json:"id,omitempty"`
	Name    string    `json:"name,omitempty"`
	Address string    `json:"address,omitempty"`
}

type AddCompany struct {
	Name    string `json:"name" binding:"required"`
	Address string `json:"address" binding:"required"`
}

type CompanyResponse struct {
	Name    string `json:"name"`
	Address string `json:"address"`
}

type GetCompanyRequest struct {
	ID string `uri:"id" binding:"required"`
}

type GetCompanyID struct {
	CompID string `uri:"compID" binding:"required,uuid"`
}
