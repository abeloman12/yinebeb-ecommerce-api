package dto

import (
	"time"

	"github.com/google/uuid"
	"github.com/shopspring/decimal"
)

type Item struct {
	ID        uuid.UUID       `json:"id"`
	ItemName  string          `json:"item_name,omitempty"`
	Price     decimal.Decimal `json:"price,omitempty"`
	MadeIn    string          `json:"made_in,omitempty"`
	CompanyID uuid.UUID       `json:"company_id,omitempty"`
	Image     string          `json:"image,omitempty"`
	CreatedAt time.Time       `json:"created_at,omitempty"`
	UpdatedAt time.Time       `json:"updated_at,omitempty"`
}
type AddItemRequest struct {
	ItemName  string          `json:"item_name,omitempty"`
	Price     decimal.Decimal `json:"price,omitempty"`
	MadeIn    string          `json:"made_in,omitempty"`
	Image     string          `json:"image,omitempty"`
	CompanyID string          `json:"company_id,omitempty"`
}

type AddItem struct {
	CompanyID uuid.UUID       `json:"company_id,omitempty"`
	ItemName  string          `json:"item_name,omitempty"`
	Price     decimal.Decimal `json:"price,omitempty"`
	MadeIn    string          `json:"made_in,omitempty"`
	Image     string          `json:"image,omitempty"`
}

type GetItemRequest struct {
	ID string `uri:"id,omitempty""`
}

type UpdateItemRequest struct {
	ID    string          `json:"id,omitempty"`
	Price decimal.Decimal `json:"price,omitempty"`
}

type UpdateItem struct {
	ID    uuid.UUID       `json:"id,omitempty"`
	Price decimal.Decimal `json:"price,omitempty"`
}
