package db

import (
	"context"
	"github.com/stretchr/testify/require"
	"simpleApi/utils"
	"testing"
	"time"
)

// Addcompany testInstance
func TestAddCompany(t *testing.T) {
	createRandomCompany(t)
}

// Getcompany testInstance
func TestGetCompany(t *testing.T) {
	company := createRandomCompany(t)
	id := company.ID
	gotcompany, err := testQueries.GetCompany(context.Background(), id)
	require.NoError(t, err)
	require.NotEmpty(t, gotcompany)

	require.Equal(t, company.Name, gotcompany.Name)
	require.Equal(t, company.Address, gotcompany.Address)
	require.WithinDuration(t, company.CreatedAt, gotcompany.CreatedAt, time.Second)
}

// return a Company for testInstance purpose.
func createRandomCompany(t *testing.T) Company {
	arg := AddCompanyParams{
		Name:    utils.RandomName(),
		Address: "Bole, Rwanda",
	}

	company, err := testQueries.AddCompany(context.Background(), arg)
	require.NoError(t, err)
	require.NotEmpty(t, company)

	require.Equal(t, arg.Name, company.Name)
	require.Equal(t, arg.Address, company.Address)

	require.NotZero(t, company.ID)
	require.NotZero(t, company.CreatedAt)

	return company
}
