package db

import (
	"context"
	"github.com/stretchr/testify/require"
	"simpleApi/utils"
	"testing"
	"time"
)

func TestUserRegistration(t *testing.T) {
	createRandomUser(t)
}

func TestUserLogin(t *testing.T) {
	user := createRandomUser(t)
	userEmail := user.Email
	gotUser, err := testQueries.GetUser(context.Background(), userEmail)
	require.NoError(t, err)
	require.NotEmpty(t, gotUser)

	require.Equal(t, user.FirstName, gotUser.FirstName)
	require.Equal(t, user.LastName, gotUser.LastName)
	require.Equal(t, user.Email, gotUser.Email)
	require.Equal(t, user.Role, gotUser.Role)
	require.Equal(t, user.HashedPassword, gotUser.HashedPassword)
	require.WithinDuration(t, user.CreatedAt, gotUser.CreatedAt, time.Second)
}

// return a random api for testInstance purpose.
func createRandomUser(t *testing.T) User {
	hashedPassword, err := utils.HashPassword(utils.RandomString(6))
	require.NoError(t, err)

	arg := CreateUserParams{
		FirstName:      utils.RandomName(),
		LastName:       utils.RandomName(),
		Email:          utils.RandomEmail(),
		HashedPassword: hashedPassword,
		Role:           utils.RandomRole(),
	}

	user, err := testQueries.CreateUser(context.Background(), arg)
	require.NoError(t, err)
	require.NotEmpty(t, user)

	require.Equal(t, arg.FirstName, user.FirstName)
	require.Equal(t, arg.LastName, user.LastName)
	require.Equal(t, arg.Email, user.Email)
	require.Equal(t, arg.HashedPassword, user.HashedPassword)
	require.Equal(t, arg.Role, user.Role)

	require.NotZero(t, user.ID)
	require.NotZero(t, user.CreatedAt)

	return user
}
