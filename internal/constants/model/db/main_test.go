package db

import (
	"context"
	"github.com/jackc/pgx/v4/pgxpool"
	"github.com/spf13/viper"
	"go.uber.org/zap"
	"os"
	"simpleApi/internal/constants/model/db/testInstance"
	"simpleApi/platforms/logger"
	"testing"
)

var testQueries *Queries
var testDB *pgxpool.Pool

func TestMain(m *testing.M) {
	var err error
	log := logger.New(testInstance.InitLogger())
	testInstance.InitConfig("config", "../../../../config", log)
	if err != nil {
		log.Info(context.Background(), "cannot load config:", zap.Error(err))
	}

	testDB, err = pgxpool.Connect(context.Background(), viper.GetString("database.url"))
	if err != nil {
		log.Info(context.Background(), "cannot connect to db", zap.Error(err))
	}

	testQueries = New(testDB)

	os.Exit(m.Run())
}
