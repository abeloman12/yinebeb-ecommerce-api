package db

import (
	"context"
	"fmt"
	"simpleApi/utils"
	"testing"
	"time"

	"github.com/google/uuid"
	"github.com/shopspring/decimal"
	"github.com/stretchr/testify/require"
)

// AddItem testInstance
func TestAddItem(t *testing.T) {
	createRandomItem(t)
}

// GetItem testInstance
func TestGetItem(t *testing.T) {
	item := createRandomItem(t)
	id := item.ID
	gotItem, err := testQueries.GetItem(context.Background(), id)
	require.NoError(t, err)
	require.NotEmpty(t, gotItem)

	require.Equal(t, item.ItemName, gotItem.ItemName)
	require.Equal(t, item.Price, gotItem.Price)
	require.Equal(t, item.Image, gotItem.Image)
	require.Equal(t, item.MadeIn, gotItem.MadeIn)
	require.WithinDuration(t, item.CreatedAt, gotItem.CreatedAt, time.Second)
}

// return a random item for testInstance purpose.
func createRandomItem(t *testing.T) Item {
	num := utils.RandomInt(100, 1000)
	arg := AddItemParams{
		CompanyID: uuid.MustParse("63d5a6a1-965c-4bbb-b9b2-fda404af1411"),
		ItemName:  utils.RandomName(),
		Price:     decimal.NewFromInt(45),
		MadeIn:    "Ethiopia",
		Image:     "assets/shoes.jpeg",
	}
	fmt.Println(arg.Price)
	fmt.Println(num)

	item, err := testQueries.AddItem(context.Background(), arg)
	require.NoError(t, err)
	require.NotEmpty(t, item)

	require.Equal(t, arg.ItemName, item.ItemName)
	require.Equal(t, arg.Price, item.Price)
	require.Equal(t, arg.Image, item.Image)
	require.Equal(t, arg.MadeIn, item.MadeIn)

	require.NotZero(t, item.ID)
	require.NotZero(t, item.CreatedAt)

	return item
}
