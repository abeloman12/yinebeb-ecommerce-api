package user

import (
	"context"
	"github.com/gin-gonic/gin"
	"simpleApi/internal/constants/errors"
	"simpleApi/internal/constants/model/dto"
	"simpleApi/internal/module"
	"simpleApi/internal/storage"
	"simpleApi/platforms/logger"
	"simpleApi/utils"

	"go.uber.org/zap"
)

type user struct {
	userPersistent storage.User
	log            logger.Logger
}

func Init(log logger.Logger, userpersistent storage.User) module.User {
	return &user{
		userPersistent: userpersistent,
		log:            log,
	}
}

func (u *user) Create(ctx context.Context, param dto.CreateUser) (*dto.User, error) {
	var err error
	if err = param.Validate(); err != nil {
		err := errors.ErrInvalidUserInput.Wrap(err, "invalid input")
		u.log.Info(ctx, "invalid input", zap.Error(err), zap.Any("input", param))
		return nil, err
	}

	hashedPassword, err := utils.HashPassword(param.Password)
	if err != nil {
		err := errors.ErrInvalidUserInput.Wrap(err, "hashing fialed")
		u.log.Info(ctx, "invalid input", zap.Error(err), zap.Any("input", param))
		return nil, err
	}
	param.Password = hashedPassword
	param.Role = "user" //default role

	return u.userPersistent.Create(ctx, param)
}

func (u *user) Get(ctx *gin.Context, param dto.LoginUserRequest) (*dto.User, error) {
	var err error
	if err = param.Validate(); err != nil {
		err := errors.ErrInvalidUserInput.Wrap(err, "invalid input")
		u.log.Info(ctx, "invalid input", zap.Error(err), zap.Any("input", param))
		return nil, err
	}
	userr, err := u.userPersistent.Get(ctx, param.Email)

	err = utils.CheckPassword(param.Password, userr.Password)
	if err != nil {
		err := errors.ErrInvalidUserInput.Wrap(err, "invalid input")
		u.log.Info(ctx, "invalid input", zap.Error(err), zap.Any("input", param))
		return nil, err
	}
	return userr, nil
}

func (u *user) UpdateRole(ctx context.Context, param dto.UpdateRole) (*dto.User, error) {
	var err error
	if err = param.Validate(); err != nil {
		err := errors.ErrInvalidUserInput.Wrap(err, "invalid input")
		u.log.Info(ctx, "invalid input", zap.Error(err), zap.Any("input", param))
		return nil, err
	}

	return u.userPersistent.UpdateRole(ctx, param)
}
