package module

import (
	"context"
	"github.com/gin-gonic/gin"
	"simpleApi/internal/constants/model/dto"
)

type User interface {
	Create(ctx context.Context, param dto.CreateUser) (*dto.User, error)
	Get(ctx *gin.Context, req dto.LoginUserRequest) (*dto.User, error)
	UpdateRole(ctx context.Context, param dto.UpdateRole) (*dto.User, error)
}

type Item interface {
	Create(ctx context.Context, param dto.AddItemRequest) (*dto.Item, error)
	Get(ctx context.Context, id string) (*dto.Item, error)
	GetAll(ctx context.Context, companyID string) ([]dto.Item, error)
	Update(ctx context.Context, param dto.UpdateItemRequest) (*dto.Item, error)
}

type Company interface {
	Create(ctx context.Context, param dto.AddCompany) (*dto.Company, error)
	Get(ctx context.Context, id string) (*dto.Company, error)
	GetAll(ctx context.Context) ([]dto.Company, error)
}
