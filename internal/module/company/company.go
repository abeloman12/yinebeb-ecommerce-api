package company

import (
	"context"
	"github.com/google/uuid"
	"go.uber.org/zap"
	"simpleApi/internal/constants/errors"
	"simpleApi/internal/constants/model/dto"
	"simpleApi/internal/module"
	"simpleApi/internal/storage"
	"simpleApi/platforms/logger"
)

type company struct {
	companyPersistent storage.Company
	log               logger.Logger
}

func Init(log logger.Logger, companypersistent storage.Company) module.Company {
	return &company{
		companyPersistent: companypersistent,
		log:               log,
	}
}

func (c *company) Create(ctx context.Context, param dto.AddCompany) (*dto.Company, error) {
	var err error
	if err = param.Validate(); err != nil {
		err := errors.ErrInvalidUserInput.Wrap(err, "invalid input")
		c.log.Info(ctx, "invalid input", zap.Error(err), zap.Any("input", param))
		return nil, err
	}
	return c.companyPersistent.Create(ctx, param)
}

func (c *company) Get(ctx context.Context, id string) (*dto.Company, error) {
	uuidID, err := uuid.Parse(id)
	if err != nil {
		err := errors.ErrInvalidUserInput.Wrap(err, "invalid id")
		c.log.Warn(ctx, "invalid id", zap.Error(err))
		return nil, err
	}
	return c.companyPersistent.Get(ctx, uuidID)
}

func (c *company) GetAll(ctx context.Context) ([]dto.Company, error) {
	return c.companyPersistent.GetAll(ctx)
}
