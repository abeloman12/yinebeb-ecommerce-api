package item

import (
	"context"
	"fmt"
	"github.com/google/uuid"
	"go.uber.org/zap"
	"simpleApi/internal/constants/errors"
	"simpleApi/internal/constants/model/dto"
	"simpleApi/internal/module"
	"simpleApi/internal/storage"
	"simpleApi/platforms/logger"
)

type item struct {
	itemPersistent storage.Item
	log            logger.Logger
}

func Init(log logger.Logger, userpersistent storage.Item) module.Item {
	return &item{
		itemPersistent: userpersistent,
		log:            log,
	}
}

func (u *item) Create(ctx context.Context, param dto.AddItemRequest) (*dto.Item, error) {
	var err error

	if err = param.Validate(); err != nil {
		err := errors.ErrInvalidUserInput.Wrap(err, "invalid input")
		u.log.Info(ctx, "invalid input", zap.Error(err), zap.Any("input", param))
		return nil, err
	}

	CuuidID := uuid.MustParse(param.CompanyID)
	fmt.Println("compID", CuuidID)
	arg := dto.AddItem{
		CompanyID: CuuidID,
		ItemName:  param.ItemName,
		Price:     param.Price,
		MadeIn:    param.MadeIn,
		Image:     param.Image,
	}
	return u.itemPersistent.Create(ctx, arg)
}

func (u *item) Get(ctx context.Context, id string) (*dto.Item, error) {
	uuidID, err := uuid.Parse(id)
	if err != nil {
		err := errors.ErrInvalidUserInput.Wrap(err, "invalid id")
		u.log.Warn(ctx, "invalid id", zap.Error(err))
		return nil, err
	}
	return u.itemPersistent.Get(ctx, uuidID)
}

func (u *item) GetAll(ctx context.Context, companyID string) ([]dto.Item, error) {
	uuidID, err := uuid.Parse(companyID)
	if err != nil {
		err := errors.ErrInvalidUserInput.Wrap(err, "invalid id")
		u.log.Warn(ctx, "invalid id", zap.Error(err))
		return nil, err
	}
	return u.itemPersistent.GetAll(ctx, uuidID)
}

func (i *item) Update(ctx context.Context, param dto.UpdateItemRequest) (*dto.Item, error) {
	var err error
	if err = param.Validate(); err != nil {
		err := errors.ErrInvalidUserInput.Wrap(err, "invalid input")
		i.log.Warn(ctx, "invalid input", zap.Error(err), zap.Any("input", param))
		return nil, err
	}

	uuidID, err := uuid.Parse(param.ID)
	if err != nil {
		err := errors.ErrInvalidUserInput.Wrap(err, "invalid id")
		i.log.Warn(ctx, "invalid id", zap.Error(err))
		return nil, err
	}
	arg := dto.UpdateItem{
		ID:    uuidID,
		Price: param.Price,
	}
	return i.itemPersistent.Update(ctx, arg)
}
