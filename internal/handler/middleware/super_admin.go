package middleware

import (
	"github.com/gin-gonic/gin"
	"simpleApi/internal/handler/helper"
)

// AuthorizeAdmin used to ensure whether we are on the right endpoint to a superAdmin.
func AuthorizeAdmin() gin.HandlerFunc {
	return func(ctx *gin.Context) {
		url := helper.ParseURL(ctx)
		if !helper.MatchRoute("/v1/superAdmin/.*", url) {
			ctx.AbortWithStatusJSON(400, gin.H{"msg": "An authorized path"})
			return
		}
		ctx.Next()
	}
}
