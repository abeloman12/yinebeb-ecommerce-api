package middleware

import (
	"fmt"
	"github.com/casbin/casbin/v2"
	"github.com/gin-gonic/gin"
	"net/http"
	"simpleApi/internal/constants"
	"simpleApi/internal/constants/model"
	"simpleApi/internal/handler/helper"
)

var dom string

// Authorize determines if current api has been authorized to take an action on an object.
func Authorize(obj string, act string, enforcer *casbin.Enforcer) gin.HandlerFunc {
	return func(c *gin.Context) {
		url := helper.ParseURL(c)
		dom = helper.SetDomain(c, url)

		// Get current api/subject
		sub, existed := c.Get("id")
		if !existed {
			c.AbortWithStatusJSON(401, gin.H{"msg": "User hasn't logged in yet"})
			return
		}

		// Load policy from Database
		err := enforcer.LoadPolicy() //total of 3 policies we had set, with a new role based policy passed via id and role.
		if err != nil {
			c.AbortWithStatusJSON(500, gin.H{"msg": "Failed to load policy from DB"})
			return
		}

		// Enforce decide whether a "subject" can access an "object" with some "action" inside some "domain".
		ok, err := enforcer.Enforce(fmt.Sprint(sub), dom, obj, act)
		if err != nil {
			c.AbortWithStatusJSON(500, gin.H{"msg": "Error occurred when authorizing api"})
			return
		}
		if !ok {
			var fieldErrors []model.FieldError
			fieldErrors = append(fieldErrors, model.FieldError{Description: "You are not authorized"})
			constants.ErrorResponse(c, &model.ErrorResponse{
				Code:       http.StatusBadRequest,
				Message:    "Invalid Input",
				FieldError: fieldErrors,
			})
		}

		c.Next()
	}
}
