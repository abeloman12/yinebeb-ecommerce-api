package company

import (
	"github.com/gin-gonic/gin"
	"go.uber.org/zap"
	"net/http"
	"simpleApi/internal/constants"
	"simpleApi/internal/constants/errors"
	"simpleApi/internal/constants/model/dto"
	"simpleApi/internal/handler/rest"
	"simpleApi/internal/module"
	"simpleApi/platforms/logger"
	"time"
)

type company struct {
	logger         logger.Logger
	companyModule  module.Company
	contextTimeout time.Duration
}

func Init(logger logger.Logger, CompanyModule module.Company, contextTimeout time.Duration) rest.Company {
	return &company{
		companyModule:  CompanyModule,
		logger:         logger,
		contextTimeout: contextTimeout,
	}
}

// // Show AddCompany godoc
// // @Summary      Adding a company
// // @Description  register a company
// // @Tags         AddCompany
// // @Accept       json
// // @Produce      json
// // @Success      200  {object}  db.Company
// // @Failure      400  {object}  map[string]string
// // @Failure		 403  {object}  map[string]string
// // @Failure      500  {object}  map[string]string
// // @Router       /addCompany [post]
func (c *company) CreateCompany(ctx *gin.Context) {
	var req dto.AddCompany
	if err := ctx.ShouldBindJSON(&req); err != nil {
		err := errors.ErrInvalidUserInput.Wrap(err, "invalid input")
		c.logger.Info(ctx, "unable to bind company data", zap.Error(err))
		_ = ctx.Error(err)
		return
	}
	arg := dto.AddCompany{
		Name:    req.Name,
		Address: req.Address,
	}
	createdcompany, err := c.companyModule.Create(ctx, arg)
	if err != nil {
		_ = ctx.Error(err)
	}
	constants.SuccessResponse(ctx, http.StatusOK, createdcompany)
}

// Show GetCompay godoc
// @Summary      getting a single company
// @Description  TODO
// @Tags         getCompany
// @Accept       json
// @Produce      json
// @Success      200  {object}  db.Company
// @Failure      400  {object}  map[string]string
// @Failure      404  {object}  map[string]string
// @Failure      500  {object}  map[string]string
// @Router       /getCompany [get]
func (c *company) GetCompany(ctx *gin.Context) {
	var req dto.GetCompanyRequest
	if err := ctx.ShouldBindJSON(&req); err != nil {
		err := errors.ErrInvalidUserInput.Wrap(err, "invalid input")
		c.logger.Info(ctx, "unable to bind company data", zap.Error(err))
		_ = ctx.Error(err)
		return
	}

	company, err := c.companyModule.Get(ctx, req.ID)
	if err != nil {
		_ = ctx.Error(err)
		return
	}
	constants.SuccessResponse(ctx, http.StatusOK, company)
}

// Show listCompany godoc
// @Summary      list of companies/vendors
// @Description  TODO
// @Tags         listCompany
// @Produce      json
// @Success      200  {object}  []db.Company
// @Failure      500  {object}  map[string]string
// @Router       /listCompany [get]
func (c *company) GetCompanies(ctx *gin.Context) {
	companies, err := c.companyModule.GetAll(ctx)
	if err != nil {
		_ = ctx.Error(err)
		return
	}
	constants.SuccessResponse(ctx, http.StatusOK, companies)
}
