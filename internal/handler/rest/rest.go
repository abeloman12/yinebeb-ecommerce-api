package rest

import (
	"github.com/casbin/casbin/v2"
	"github.com/gin-gonic/gin"
)

type User interface {
	CreateUser(enforcer *casbin.Enforcer) gin.HandlerFunc
	Login(ctx *gin.Context)
	AddPolicy(enforcer *casbin.Enforcer) gin.HandlerFunc
	AddGroupPolicy(enforcer *casbin.Enforcer) gin.HandlerFunc
}

type Item interface {
	CreateItem(ctx *gin.Context)
	GetItem(ctx *gin.Context)
	GetItems(ctx *gin.Context)
	UpdateItem(ctx *gin.Context)
}

type Company interface {
	CreateCompany(ctx *gin.Context)
	GetCompany(ctx *gin.Context)
	GetCompanies(ctx *gin.Context)
}
