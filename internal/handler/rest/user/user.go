package user

import (
	"github.com/casbin/casbin/v2"
	"github.com/gin-gonic/gin"
	"go.uber.org/zap"
	"net/http"
	"simpleApi/internal/constants"
	"simpleApi/internal/constants/errors"
	"simpleApi/internal/constants/model"
	"simpleApi/internal/constants/model/dto"
	"simpleApi/internal/handler/helper"
	"simpleApi/internal/handler/rest"
	"simpleApi/internal/module"
	"simpleApi/platforms/logger"
	"simpleApi/utils"
	"time"
)

type user struct {
	logger         logger.Logger
	userModule     module.User
	contextTimeout time.Duration
}

func Init(logger logger.Logger, userModule module.User, contextTimeout time.Duration) rest.User {
	return &user{
		userModule:     userModule,
		logger:         logger,
		contextTimeout: contextTimeout,
	}
}

// Show CreateUser godoc
// @Summary      creating a api
// @Description  register a api by sending json
// @Tags         CreateUser
// @Accept       json
// @Produce      json
// @Success      200  {object}  dto.UserResponse
// @Failure      400  {object}  map[string]string
// @Failure		 403  {object}  map[string]string
// @Failure      404  {object}  map[string]string
// @Failure      500  {object}  map[string]string
// @Router       /registration [post]
func (u *user) CreateUser(enforcer *casbin.Enforcer) gin.HandlerFunc {
	return func(ctx *gin.Context) {
		url := helper.ParseURL(ctx)
		dom := helper.SetDomain(ctx, url)
		var req dto.CreateUser
		if err := ctx.ShouldBindJSON(&req); err != nil {
			if err := req.Validate(); err != nil {
				err := errors.ErrInvalidUserInput.Wrap(err, "invalid input")
				u.logger.Info(ctx, "unable to bind user data", zap.Error(err))
				_ = ctx.Error(err) //what it is real this?
				return
			}
		}
		createduser, err := u.userModule.Create(ctx, req)
		if err != nil {
			_ = ctx.Error(err)
			return
		}
		//check if the api is superAdmin and add a policy
		if url == "/v1/superAdmin/registration" {
			_, err := enforcer.AddGroupingPolicy(createduser.ID.String(), "admin", "system")
			if err != nil {
				constants.ErrorResponse(ctx, &model.ErrorResponse{
					Code:    http.StatusInternalServerError,
					Message: err.Error(),
				})
				return
			}
			//update the default role
			arg := dto.UpdateRole{
				ID:   createduser.ID,
				Role: "admin",
			}
			_, err = u.userModule.UpdateRole(ctx, arg)
			if err != nil {
				_ = ctx.Error(err)
				return
			}
		}

		//add default group policy as "user" role on some domain
		_, err = enforcer.AddGroupingPolicy(createduser.ID.String(), "user", dom)
		if err != nil {
			constants.ErrorResponse(ctx, &model.ErrorResponse{
				Code:    http.StatusInternalServerError,
				Message: err.Error(),
			})
			return
		}
		rsp := helper.NewUserResponse(createduser)
		constants.SuccessResponse(ctx, http.StatusOK, rsp)
	}
}

// Show UserLogin godoc
// @Summary      logging in a api
// @Description  logged in a api by sending a credentials
// @Tags         logInUser
// @Accept       json
// @Produce      json
// @Success      200  {object}  dto.UserResponse
// @Failure      400  {object}  map[string]string
// @Failure      401  {object}  map[string]string
// @Failure      404  {object}  map[string]string
// @Failure      500  {object}  map[string]string
// @Router       /login [post]
func (u *user) Login(ctx *gin.Context) {
	url := helper.ParseURL(ctx)
	compId := helper.SetToken(ctx, url) //set a token to manage company scope

	var req dto.LoginUserRequest
	if err := ctx.ShouldBindJSON(&req); err != nil {
		err := errors.ErrInvalidUserInput.Wrap(err, "invalid input")
		u.logger.Info(ctx, "unable to bind user login data", zap.Error(err))
		_ = ctx.Error(err)
		return
	}
	user, err := u.userModule.Get(ctx, req)
	if err != nil {
		_ = ctx.Error(err)
		return
	}

	token := utils.GenerateToken(user.ID, compId)
	rsp := dto.LogInResponse{
		Name:  user.FirstName,
		Token: token,
	}
	constants.LoginSuccessResponse(ctx, http.StatusOK, rsp)
}
