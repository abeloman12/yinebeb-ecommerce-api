package user

import (
	"database/sql"
	"github.com/casbin/casbin/v2"
	"github.com/gin-gonic/gin"
	"github.com/google/uuid"
	"net/http"
	"simpleApi/internal/constants"
	m "simpleApi/internal/constants/model"
	"simpleApi/internal/constants/model/dto"
)

// AddGroupPolicy add a group policy to casbin_rule by associating user_ID and user_Role with some domain
// Show addGroupPolicy godoc
// @Summary      adding a role to ...
// @Description  abc xyz
// @Tags         addGroupPolicy
// @Produce      json
// @Success      200  {object}  []db.Item
// @Failure      500  {object}  map[string]string
// @Router       /system/addGroupPolicy [post]
func (u *user) AddGroupPolicy(enforcer *casbin.Enforcer) gin.HandlerFunc {
	return func(ctx *gin.Context) {
		var req dto.AddGroupPolicy
		if err := ctx.ShouldBindJSON(&req); err != nil {
			constants.ErrorResponse(ctx, &m.ErrorResponse{
				Code:    http.StatusBadRequest,
				Message: err.Error(),
			})
			return
		}
		//TODO check the specified domain and api exist,before adding group policy
		_, err := enforcer.AddGroupingPolicy(req.ID, req.Role, req.Domain)
		if err != nil {
			constants.ErrorResponse(ctx, &m.ErrorResponse{
				Code:    http.StatusInternalServerError,
				Message: err.Error(),
			})
			return
		}
		idd, _ := uuid.Parse(req.ID)
		//Update the default role
		arg := dto.UpdateRole{
			ID:   idd,
			Role: req.Role,
		}
		_, err = u.userModule.UpdateRole(ctx, arg)
		if err != nil {
			if err == sql.ErrNoRows {
				constants.ErrorResponse(ctx, &m.ErrorResponse{
					Code:    http.StatusNotFound,
					Message: err.Error(),
				})
				return
			}
			constants.ErrorResponse(ctx, &m.ErrorResponse{
				Code:    http.StatusInternalServerError,
				Message: err.Error(),
			})
			return
		}
		ctx.JSON(http.StatusOK, gin.H{"msg": "Group policy added!"})
	}
}

// AddPolicy register a set of permission rules following sub,dom,obj,act
func (u *user) AddPolicy(enforcer *casbin.Enforcer) gin.HandlerFunc {
	return func(ctx *gin.Context) {
		var req dto.AddPolicyRequest
		if err := ctx.ShouldBindJSON(&req); err != nil {
			constants.ErrorResponse(ctx, &m.ErrorResponse{
				Code:    http.StatusBadRequest,
				Message: err.Error(),
			})
			return
		}

		rule := []string{req.Role, req.Domain, req.Object, req.Action}
		_, err := enforcer.AddPolicy(rule)
		if err != nil {
			constants.ErrorResponse(ctx, &m.ErrorResponse{
				Code:    http.StatusInternalServerError,
				Message: err.Error(),
			})
			return
		}
		ctx.JSON(http.StatusOK, gin.H{"msg": "policy added"})
	}
}
