package item

import (
	"fmt"
	"github.com/gin-gonic/gin"
	"github.com/google/uuid"
	"go.uber.org/zap"
	"net/http"
	"simpleApi/internal/constants"
	"simpleApi/internal/constants/errors"
	"simpleApi/internal/constants/model/dto"
	"simpleApi/internal/handler/rest"
	"simpleApi/internal/module"
	"simpleApi/platforms/logger"
	"time"
)

type item struct {
	logger         logger.Logger
	itemModule     module.Item
	contextTimeout time.Duration
}

func Init(logger logger.Logger, ItemModule module.Item, contextTimeout time.Duration) rest.Item {
	return &item{
		itemModule:     ItemModule,
		logger:         logger,
		contextTimeout: contextTimeout,
	}
}

// Show AddItem godoc
// @Summary      Adding an item
// @Description  register a api by sending a json
// @Tags         AddItem
// @Accept       json
// @Produce      json
// @Success      200  {object}  db.Item
// @Failure      400  {object}  map[string]string
// @Failure		 403  {object}  map[string]string
// @Failure      500  {object}  map[string]string
// @Router       /addItem [post]
func (i *item) CreateItem(ctx *gin.Context) {
	var req dto.AddItemRequest
	if err := ctx.ShouldBindJSON(&req); err != nil {
		err := errors.ErrInvalidUserInput.Wrap(err, "invalid input")
		i.logger.Info(ctx, "unable to bind item data", zap.Error(err))
		_ = ctx.Error(err)
		return
	}
	companyId, _ := ctx.Get("compID")
	arg := dto.AddItemRequest{
		ItemName:  req.ItemName,
		CompanyID: fmt.Sprint(companyId),
		MadeIn:    req.MadeIn,
		Price:     req.Price,
		Image:     req.Image,
	}

	createditem, err := i.itemModule.Create(ctx, arg)
	if err != nil {
		_ = ctx.Error(err)
		return
	}

	constants.SuccessResponse(ctx, http.StatusOK, createditem)
}

// Show GetItem godoc
// @Summary      getting a single item
// @Description  logged in a api by sending a credentials
// @Tags         getItem
// @Accept       json
// @Produce      json
// @Success      200  {object}  db.Item
// @Failure      400  {object}  map[string]string
// @Failure      404  {object}  map[string]string
// @Failure      500  {object}  map[string]string
// @Router       /getItem [get]
func (i *item) GetItem(ctx *gin.Context) {
	var req dto.GetItemRequest
	if err := ctx.ShouldBindJSON(&req); err != nil {
		err := errors.ErrInvalidUserInput.Wrap(err, "invalid input")
		i.logger.Info(ctx, "unable to bind item data", zap.Error(err))
		_ = ctx.Error(err)
		return
	}

	itemE, err := i.itemModule.Get(ctx, req.ID)
	if err != nil {
		_ = ctx.Error(err)
		return
	}
	comp, _ := ctx.Get("compID")
	if itemE.CompanyID != uuid.MustParse(fmt.Sprint(comp)) {
		err := errors.ErrInvalidUserInput.Wrap(err, "Item doesn't belong to this user domain")
		i.logger.Info(ctx, "Item doesn't belong to this user domain", zap.Error(err))
		_ = ctx.Error(err)
		return
	}
	constants.SuccessResponse(ctx, http.StatusOK, itemE)
}

// Show listItem godoc
// @Summary      list of item
// @Description  abc...xyz...
// @Tags         listItems
// @Produce      json
// @Success      200  {object}  []db.Item
// @Failure      500  {object}  map[string]string
// @Router       /listItem [get]
func (i *item) GetItems(ctx *gin.Context) {
	compId, _ := ctx.Get("compID")
	items, err := i.itemModule.GetAll(ctx, fmt.Sprint(compId))
	if err != nil {
		_ = ctx.Error(err)
		return
	}
	constants.SuccessResponse(ctx, http.StatusOK, items)
}

// Show updateItem godoc
// @Summary      update an item via its id
// @Description  TODO
// @Tags         updateItem
// @Produce      json
// @Success      200  {object}  db.Item
// @Failure      500  {object}  map[string]string
// @Router       /updateItem [post]
func (i *item) UpdateItem(ctx *gin.Context) {
	var req dto.UpdateItemRequest
	if err := ctx.ShouldBindJSON(&req); err != nil {
		err := errors.ErrInvalidUserInput.Wrap(err, "invalid input")
		i.logger.Info(ctx, "unable to bind updateItem data", zap.Error(err))
		_ = ctx.Error(err)
		return
	}

	item, _ := i.itemModule.Get(ctx, req.ID)
	comp, _ := ctx.Get("compID")
	if item.CompanyID != uuid.MustParse(fmt.Sprint(comp)) {
		err := errors.ErrInvalidUserInput.Wrap(nil, "Item doesn't belong to this api domain")
		i.logger.Info(ctx, "Item doesn't belong to this api domain", zap.Error(err))
		_ = ctx.Error(err)
		return
	}

	updateditem, err := i.itemModule.Update(ctx, req)
	if err != nil {
		_ = ctx.Error(err)
		return
	}
	constants.SuccessResponse(ctx, http.StatusCreated, updateditem)
}
