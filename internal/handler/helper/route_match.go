package helper

import (
	"github.com/gin-gonic/gin"
	"net/http"
	"regexp"
	"simpleApi/internal/constants"
	m "simpleApi/internal/constants/model"
	"simpleApi/internal/constants/model/dto"
)

// ParseURL return the request URL path.
func ParseURL(ctx *gin.Context) string {
	url := ctx.Request.URL.String()
	return url
}

// MatchRoute matches a route with a given particular relative_path
func MatchRoute(relative_path, route string) bool {
	re, _ := regexp.Compile(relative_path)
	if !re.Match([]byte(route)) {
		return false
	}
	return true
}

// SetDomain is a helper function to Authorize middleware, used to set the domain
func SetDomain(c *gin.Context, url string) string {
	var domain string
	if MatchRoute("/company/.*", url) {
		var req dto.GetCompanyID
		if err := c.ShouldBindUri(&req); err != nil {
			constants.ErrorResponse(c, &m.ErrorResponse{
				Code:    http.StatusBadRequest,
				Message: err.Error(),
			})
		}
		domain = req.CompID
	}
	if MatchRoute("/customer/.*", url) {
		domain = "customer"
	}
	if MatchRoute("/system/.*", url) {
		domain = "system"
	}
	return domain
}

// SetToken is a helper function to Authorize middleware, used to set the domain
func SetToken(c *gin.Context, url string) string {
	if MatchRoute("/company/.*", url) {
		var req dto.GetCompanyID
		if err := c.ShouldBindUri(&req); err != nil {
			constants.ErrorResponse(c, &m.ErrorResponse{
				Code:    http.StatusBadRequest,
				Message: err.Error(),
			})
		}
		return req.CompID
	}
	return ""
}

func NewUserResponse(user *dto.User) dto.UserResponse {
	rsp := dto.UserResponse{
		ID:        user.ID.String(),
		FirstName: user.FirstName,
		LastName:  user.LastName,
		Email:     user.Email,
		CreatedAt: user.CreatedAt,
	}
	return rsp
}
