package item

import (
	"github.com/casbin/casbin/v2"
	"github.com/gin-gonic/gin"
	"net/http"
	"simpleApi/internal/glue/routing"
	middleware2 "simpleApi/internal/handler/middleware"
	"simpleApi/internal/handler/rest"
)

func InitRoute(grp *gin.RouterGroup, item rest.Item, enforcer *casbin.Enforcer) {
	//item
	compUsers := grp.Group("company/:compID")
	compUserRoutes := []routing.Router{
		{
			Method:      http.MethodPost,
			Path:        "/addItem",
			Handler:     item.CreateItem,
			Middlewares: []gin.HandlerFunc{middleware2.AuthorizeJwt(), middleware2.Authorize("item", "write", enforcer)},
		},
		{
			Method:      http.MethodGet,
			Path:        "/getItem/:id",
			Handler:     item.GetItem,
			Middlewares: []gin.HandlerFunc{middleware2.AuthorizeJwt(), middleware2.Authorize("item", "read", enforcer)},
		},
		{
			Method:      http.MethodGet,
			Path:        "/listItem",
			Handler:     item.GetItems,
			Middlewares: []gin.HandlerFunc{middleware2.AuthorizeJwt(), middleware2.Authorize("item", "read", enforcer)},
		},
		{
			Method:      http.MethodPost,
			Path:        "/updateItem",
			Handler:     item.UpdateItem,
			Middlewares: []gin.HandlerFunc{middleware2.AuthorizeJwt(), middleware2.Authorize("item", "write", enforcer)},
		},
	}
	routing.RegisterRoutes(compUsers, compUserRoutes)

	cumusers := grp.Group("/customer")
	cumusersRoutes := []routing.Router{
		{
			Method:      http.MethodGet,
			Path:        "/getItem/:id",
			Handler:     item.GetItem,
			Middlewares: []gin.HandlerFunc{middleware2.AuthorizeJwt(), middleware2.Authorize("item", "read", enforcer)},
		},
		{
			Method:      http.MethodGet,
			Path:        "/listItem",
			Handler:     item.GetItems,
			Middlewares: []gin.HandlerFunc{middleware2.AuthorizeJwt(), middleware2.Authorize("item", "read", enforcer)},
		},
	}
	routing.RegisterRoutes(cumusers, cumusersRoutes)
}
