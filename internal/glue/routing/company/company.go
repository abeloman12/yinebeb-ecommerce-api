package company

import (
	"github.com/casbin/casbin/v2"
	"github.com/gin-gonic/gin"
	"net/http"
	"simpleApi/internal/glue/routing"
	middleware2 "simpleApi/internal/handler/middleware"
	"simpleApi/internal/handler/rest"
)

func InitRoute(grp *gin.RouterGroup, company rest.Company, enforcer *casbin.Enforcer) {
	systemUser := grp.Group("/system")
	systemUserRoutes := []routing.Router{

		{
			Method:      http.MethodPost,
			Path:        "/addCompany",
			Handler:     company.CreateCompany,
			Middlewares: []gin.HandlerFunc{middleware2.AuthorizeJwt(), middleware2.Authorize("item", "write", enforcer)},
		},
		{
			Method:      http.MethodGet,
			Path:        "/getCompany/:id",
			Handler:     company.GetCompany,
			Middlewares: []gin.HandlerFunc{middleware2.AuthorizeJwt(), middleware2.Authorize("item", "read", enforcer)},
		},
		{
			Method:      http.MethodGet,
			Path:        "/listCompany",
			Handler:     company.GetCompanies,
			Middlewares: []gin.HandlerFunc{middleware2.AuthorizeJwt(), middleware2.Authorize("item", "read", enforcer)},
		},
	}
	routing.RegisterRoutes(systemUser, systemUserRoutes)

}
