package user

import (
	"github.com/casbin/casbin/v2"
	"github.com/gin-gonic/gin"
	"net/http"
	"simpleApi/internal/glue/routing"
	middleware2 "simpleApi/internal/handler/middleware"
	"simpleApi/internal/handler/rest"
)

func InitRoute(grp *gin.RouterGroup, user rest.User, enforcer *casbin.Enforcer) {
	//user
	admin := grp.Group("")
	adminRoutes := []routing.Router{
		{
			Method:      http.MethodPost,
			Path:        "/superAdmin/registration",
			Handler:     user.CreateUser(enforcer),
			Middlewares: []gin.HandlerFunc{middleware2.AuthorizeAdmin()},
		},
		{
			Method:  http.MethodPost,
			Path:    "/login",
			Handler: user.Login,
		},
	}
	routing.RegisterRoutes(admin, adminRoutes)

	systemUser := grp.Group("/system")
	systemUserRoutes := []routing.Router{
		{
			Method:      http.MethodPost,
			Path:        "/addGroupPolicy",
			Handler:     user.AddGroupPolicy(enforcer),
			Middlewares: []gin.HandlerFunc{middleware2.AuthorizeJwt(), middleware2.Authorize("item", "write", enforcer)},
		},
		{
			Method:      http.MethodPost,
			Path:        "/addPolicy",
			Handler:     user.AddPolicy(enforcer),
			Middlewares: []gin.HandlerFunc{middleware2.AuthorizeJwt(), middleware2.Authorize("item", "write", enforcer)},
		},
	}
	routing.RegisterRoutes(systemUser, systemUserRoutes)

	users := grp.Group("/customer")
	usersRoutes := []routing.Router{
		{
			Method:  http.MethodPost,
			Path:    "/registration",
			Handler: user.CreateUser(enforcer),
		},
	}
	routing.RegisterRoutes(users, usersRoutes)

	compUsers := grp.Group("company/:compID")
	compUserRoutes := []routing.Router{
		{
			Method:  http.MethodPost,
			Path:    "/registration",
			Handler: user.CreateUser(enforcer),
		},
		{
			Method:  http.MethodPost,
			Path:    "/login",
			Handler: user.Login,
		},
	}
	routing.RegisterRoutes(compUsers, compUserRoutes)
}
