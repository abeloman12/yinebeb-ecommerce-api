package initiator

import (
	"fmt"
	"log"
	"simpleApi/platforms/pgxadaptor"

	"github.com/casbin/casbin/v2"
	"github.com/jackc/pgx/v4/pgxpool"
)

// PermConfig return an Enforcer after initiating minimal policy rules
func PermConfig(dbconn *pgxpool.Pool, path string) *casbin.Enforcer {
	// Initialize  casbin adapter, this will create a table named casbin_rule
	pgxadapter, err := pgxadaptor.NewAdapterWithDB(dbconn)
	if err != nil {
		panic(fmt.Sprintf("failed to initialize casbin adapter: %v", err))
	}

	// Load model configuration file and policy store adapter
	enforcer, err := casbin.NewEnforcer(path, pgxadapter)
	if err != nil {
		panic(fmt.Sprintf("failed to create casbin enforcer: %v", err))
	}

	rule := [][]string{
		//predefined policies on system
		{"admin", "system", "item", "read"},
		{"admin", "system", "item", "write"},
		{"user", "system", "item", "read"},

		//predefined policy inside customer
		{"user", "customer", "item", "read"},

		//policies on company will add dynamically via addPolicy handler upon request
	}
	//add policy
	_, err = enforcer.AddPolicies(rule)
	if err != nil {
		log.Println("Error occurred while adding policies")
		return nil
	}
	return enforcer
}
