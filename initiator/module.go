package initiator

import (
	"simpleApi/internal/module"
	"simpleApi/internal/module/company"
	"simpleApi/internal/module/item"
	"simpleApi/internal/module/user"
	"simpleApi/platforms/logger"
)

type Module struct {
	user    module.User
	item    module.Item
	company module.Company
}

func InitModule(persistence Persistence, log logger.Logger) Module {
	return Module{
		user:    user.Init(log.Named("user-module"), persistence.user),
		item:    item.Init(log.Named("item-module"), persistence.item),
		company: company.Init(log.Named("company-module"), persistence.company),
	}
}
