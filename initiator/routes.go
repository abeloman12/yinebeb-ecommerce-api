package initiator

import (
	"fmt"
	"simpleApi/docs"
	"simpleApi/internal/glue/routing/company"
	"simpleApi/internal/glue/routing/item"
	"simpleApi/internal/glue/routing/user"

	"github.com/casbin/casbin/v2"
	"github.com/gin-gonic/gin"
	"github.com/spf13/viper"
	swaggerFiles "github.com/swaggo/files"
	ginSwagger "github.com/swaggo/gin-swagger"
)

func InitRouter(group *gin.RouterGroup, handler Handler, enforcer *casbin.Enforcer) {
	docs.SwaggerInfo.Host = fmt.Sprintf("%s:%s", viper.GetString("server.host"), viper.GetString("server.port"))
	docs.SwaggerInfo.BasePath = "/v1"
	group.GET("/swagger/*any", ginSwagger.WrapHandler(swaggerFiles.Handler))

	user.InitRoute(group, handler.user, enforcer)
	item.InitRoute(group, handler.item, enforcer)
	company.InitRoute(group, handler.company, enforcer)
}
