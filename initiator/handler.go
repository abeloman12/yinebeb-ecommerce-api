package initiator

import (
	"simpleApi/internal/handler/rest"
	"simpleApi/internal/handler/rest/company"
	"simpleApi/internal/handler/rest/item"
	"simpleApi/internal/handler/rest/user"
	"simpleApi/platforms/logger"
	"time"
)

type Handler struct {
	user    rest.User
	item    rest.Item
	company rest.Company
}

func InitHandler(module Module, log logger.Logger, timeout time.Duration) Handler {
	return Handler{
		user:    user.Init(log.Named("user-handler"), module.user, timeout),
		item:    item.Init(log.Named("item-handler"), module.item, timeout),
		company: company.Init(log.Named("company-handler"), module.company, timeout),
	}
}
