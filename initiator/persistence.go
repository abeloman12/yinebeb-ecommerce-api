package initiator

import (
	"simpleApi/internal/constants/dbinstance"
	//"simpleApi/internal/handler/rest/item"
	"simpleApi/internal/storage"
	"simpleApi/internal/storage/persistence/company"
	"simpleApi/internal/storage/persistence/item"
	"simpleApi/internal/storage/persistence/user"

	"simpleApi/platforms/logger"
)

type Persistence struct {
	user    storage.User
	item    storage.Item
	company storage.Company
}

func InitPersistence(db dbinstance.DBInstance, log logger.Logger) Persistence {
	return Persistence{
		user:    user.Init(db, log.Named("user-persistence")),
		item:    item.Init(db, log.Named("item-persistence")),
		company: company.Init(db, log.Named("company-persistence")),
	}
}
