package utils

import (
	"github.com/stretchr/testify/require"
	"testing"
)

func TestPassword(t *testing.T) {
	password := "secret"
	hashedP, err := HashPassword(password)
	require.NoError(t, err)
	require.NotEmpty(t, hashedP)

	err = CheckPassword(password, hashedP)
	require.NoError(t, err)
}
